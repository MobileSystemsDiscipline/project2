package com.example.mobilesystems_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FirstActivity extends AppCompatActivity {

    public final static String USERNAME = "com.example.mobilesystems_2.username";
    public final static String GIFT = "com.example.mobilesystems_2.gift";
    public final static int REQUEST_CODE = 17;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        Button bSend = (Button) findViewById(R.id.bSend);
        bSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText senderEditText = (EditText) findViewById(R.id.editText1);
                EditText giftEditText = (EditText) findViewById(R.id.editText2);

                Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
                intent.putExtra(USERNAME, senderEditText.getText().toString());
                intent.putExtra(GIFT, giftEditText.getText().toString());

                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TextView infoTextView = (TextView) findViewById(R.id.textViewThank);

        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                infoTextView.setText(data.getStringExtra(SecondActivity.REQUEST_STRING));
            }
            else {
                infoTextView.setText("");
            }
        }
    }
}
