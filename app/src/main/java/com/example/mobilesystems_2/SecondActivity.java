package com.example.mobilesystems_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static com.example.mobilesystems_2.FirstActivity.GIFT;
import static com.example.mobilesystems_2.FirstActivity.USERNAME;

public class SecondActivity extends AppCompatActivity {

    public final static String REQUEST_STRING = "com.example.mobilesystems_2.request";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String sender = getIntent().getStringExtra(USERNAME);
        String gift = getIntent().getStringExtra(GIFT);

        TextView textView = (TextView) findViewById(R.id.tV);
        textView.setText(getResources().getText(R.string.sender)  + " " + sender + " прислал вам: " + gift);

        Button bThanks = (Button) findViewById(R.id.buttonThanks);
        bThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent answerIntent = new Intent();
                answerIntent.putExtra(REQUEST_STRING, "СПАСИБО!");
                setResult(RESULT_OK, answerIntent);
                finish();
            }
        });


    }
}